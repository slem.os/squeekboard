Name:           squeekboard
Version:        1.9.3
Release:        2%{?dist}
Summary:        a Wayland virtual keyboard

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/squeekboard
Source0:        https://source.puri.sm/Librem5/squeekboard/-/archive/v%{version}/squeekboard-v%{version}.tar.gz
Source1:        squeekboard.desktop
Source2:        vendor.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  cargo
BuildRequires:  rust-packaging
BuildRequires:  rust
BuildRequires:  pkgconfig(gio-2.0) >= 2.26
BuildRequires:  pkgconfig(gnome-desktop-3.0) >= 3.0
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.0
BuildRequires:  pkgconfig(libcroco-0.6)
BuildRequires:  pkgconfig(wayland-client) >= 1.14
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  pkgconfig(wayland-protocols) >= 1.12
BuildRequires:  desktop-file-utils
BuildRequires:  feedbackd-devel


%description
Squeekboard is a virtual keyboard supporting Wayland, built primarily 
for the Librem 5 phone. It squeaks because some Rust got inside.


%prep
%setup -q -a 0 -a 2 -n squeekboard-v%{version}
mkdir cargo-home
cat >cargo-home/config <<EOF
[source.crates-io]
registry = 'https://github.com/rust-lang/crates.io-index'
replace-with = 'vendored-sources'
[source.vendored-sources]
directory = './vendor'
EOF

%build
export CARGO_HOME=`pwd`/cargo-home/
%meson
%meson_build
#meson . build/ -Ddepdatadir=%{buildroot}/usr/share

%install
export CARGO_HOME=`pwd`/cargo-home/
# DESTDIR=%{buildroot} ninja -C _build install
%meson_install
mkdir -p %{buildroot}/%{_sysconfdir}/xdg/autostart/
cp %{SOURCE1} %{buildroot}/%{_sysconfdir}/xdg/autostart/
chmod +x %{buildroot}/%{_bindir}/squeekboard-entry

%files
%{_bindir}/squeekboard
%{_bindir}/squeekboard-entry
%{_bindir}/squeekboard-test-layout
%{_datadir}/applications/sm.puri.Squeekboard.desktop
%{_sysconfdir}/xdg/autostart/squeekboard.desktop
%doc README.md
%license COPYING


%changelog
* Sat Jun 20 2020 Adrian Campos <adriancampos@teachelp.com> - 1.9.0-1
- Initial packaging

